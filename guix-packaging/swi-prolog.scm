(define-public swi-prolog
  (package
    (name "swi-prolog")
    (version "9.0.4")
    (source (origin
              (method url-fetch)
              (uri (list
                    (string-append
                     "https://www.swi-prolog.org/download/stable/src/swipl-"
                     version
                     ".tar.gz")))
              (sha256
               (base32
                "1w29ifm0nx9r2935068w3mz5m4q50j1lk0afncfahkyka5d83cpy"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
       ;; XXX Create another output and install documentation
       (list "-DINSTALL_DOCUMENTATION=OFF")
       #:phases
       (modify-phases %standard-phases
         ;; Delete the test phase that attempts to write to the immutable store
	 ;; XXX see if we can remove tests with fewer modifications
         (add-after 'unpack 'delete-failing-tests
           (lambda _
             (substitute* "src/CMakeLists.txt"
               ((" save") ""))
             (substitute* "src/test.pl"
               (("testdir\\('Tests/save'\\).") ""))
             (delete-file-recursively "src/Tests/save"))))))
    ;; TODO ensure native and inputs are what we need
    (native-inputs (list
                    texinfo
                    perl
                    pkg-config))
    (inputs (list
             unixodbc
             openssl
             zlib
             gmp
             readline
             libunwind
             libjpeg-turbo
             pkg-config
             libxft
             fontconfig))
    (home-page "https://www.swi-prolog.org/")
    (synopsis "ISO/Edinburgh-style Prolog interpreter")
    (description "SWI-Prolog is a fast and powerful ISO/Edinburgh-style Prolog
compiler with a rich set of built-in predicates.  It offers a fast, robust and
small environment which enables substantial applications to be developed with
it.")
    (license license:bsd-2)))
