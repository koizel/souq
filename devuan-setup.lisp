(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload (list :with-user-abort :adopt) :silent t))

(defpackage :devuan-setup
  (:use #:cl)
  (:export #:toplevel #:*ui*))

(in-package :devuan-setup)

(defparameter *user* "g")

(defun install (&rest pkgs)
  (uiop:run-program (append (list "apt" "install" "--quiet" "--yes") pkgs)
                    :output :interactive :input :interactive))

(defun adduser (grp &optional (user *user*))
  (uiop:run-program (list "usermod" "-aG" grp user)))

(defmacro exit-on-ctrl-c (&body body)
  `(handler-case (with-user-abort:with-user-abort
                   (progn
                     ,@body))
     (with-user-abort:user-abort ()
       (sb-ext:exit :code 130))))

(defparameter *ui*
  (adopt:make-interface
   :name "devuan-setup"
   :summary "Setup a fresh Devuan system"
   :usage "devuan-setup"
   :help ""))

(defun toplevel ()
  (sb-ext:disable-debugger)
  (exit-on-ctrl-c
    (install "connman" "udisks2")
    (install "rlwrap")
    (install "wlr-randr" "wl-clipboard" "dunst" "wlsunset" "alacritty"
             "light" "kanshi")
    (install "dex" "zoxide" "direnv" "fzf" "imv" "urlscan"
             "fonts-ipafont" "fonts-hanazono")
    (install "pipewire" "wireplumber" "pipewire-pulse")

    ;; Virtualisation
    (install "qemu-system" "libvirt-daemon-system")
    (adduser "libvirt")))
