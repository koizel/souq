;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.

;; Ryzen 3 processors can't display anything without
;; proprietary drivers unless "uvesafb" is loaded, see
;; https://lists.gnu.org/archive/html/help-guix/2020-11/msg00172.html
;; All sections that pertain to that matter are marked
;; "Ryzen3/uvesafb".


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
             ;; For borg
             (gnu packages backup)
             ;; For v86d
             (gnu packages xorg))
(use-service-modules networking ssh 
                     ;; For elogind
                     desktop
                     ;; For kernel-module-loader-service-type
                     linux)

(define (join-newline keys)
  "Print aesthetically the content of KEYS with a newline between each element."
  (format #f "~{~a~^\n~}" keys))

(operating-system
 (locale "en_US.utf8")
 (timezone "Europe/Paris")
 (keyboard-layout (keyboard-layout "us" "workman"))
 (host-name "rufus")

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "r")
                (comment "Rufus")
                (group "users")
                (home-directory "/home/r")
                (supplementary-groups '("wheel" "netdev" "audio" "video")))
               (user-account
                (name "gabriel")
                (group "users")
                (home-directory "/pool/home/gabriel"))
               (user-account
                (name "ludovic")
                (group "users")
                (home-directory "/pool/home/ludovic"))
               (user-account
                (name "cho")
                (group "users")
                (home-directory "/pool/home/cho"))
               %base-user-accounts))

 ;; Packages installed system-wide.  Users can also install packages
 ;; under their own account: use 'guix search KEYWORD' to search
 ;; for packages and 'guix install PACKAGE' to install a package.
 (packages (append (list borg (specification->package "nss-certs"))
                   %base-packages))

 ;; Ryzen3/uvesafb
 (kernel-arguments (list "nomodeset"))

 ;; Below is the list of system services.  To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
  (append (list

           ;; To configure OpenSSH, pass an 'openssh-configuration'
           ;; record as a second argument to 'service' below.
           (service openssh-service-type
                    (openssh-configuration
                     (password-authentication? #f)
                     (authorized-keys
                      `(("r"
                         ,(mixed-text-file
                           "r.pub"
                           (join-newline
                            (list
                             "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBK5b4drZnI2VY/mQ3/DyN5GTPFEpPSbiDPGgFd76ssD941mSoTvtCJQDU4OKJFgigdfsCzRgUl5EpIGil+nMzX4= g@eryngii"
                             "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAF5tHk9QgKbvUHNL+3o5rrw6laIvAroJQo/0lbpIV3Y gabriel@motacilla"))))
                        ("gabriel"
                         ,(mixed-text-file
                           "gabriel.pub"
                           (join-newline
                            (list
                             "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG0hpBBivKN5lxHXI6nCoUMAW+zMtXyC5mePgMM11BhI g@eryngii"
                             "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAF5tHk9QgKbvUHNL+3o5rrw6laIvAroJQo/0lbpIV3Y gabriel@motacilla"))))
			("cho"
			 ,(mixed-text-file
			    "cho.pub"
			    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEpd38NX6F+in5Lpj1R/dm/ERnN8PTLQe6hlO9omXtQ7 cho@trutta"))))))
           (service dhcp-client-service-type)
           (service ntp-service-type)
           ;; elogind handles proper halt on power button press
           (service elogind-service-type)
           ;; Ryzen3/uvesafb
	   (service kernel-module-loader-service-type (list "uvesafb"))
	   (simple-service 'uvesafb-configuration etc-service-type
			   (list `("modprobe.d/uvesafb.conf"
				   ,(mixed-text-file
				     "uvesafb.conf"
				     "options uvesafb v86d=" v86d
				     "/sbin/v86d mode_option=1024x768\n")))))

          ;; This is the default list of services we
          ;; are appending to.
          %base-services))
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))

 ;; Disks which contain irrelevant data are referenced by labels, so
 ;; that any partition with the same label can be used instead.
 ;; These partitions are the one whose content is solely defined by
 ;; the present configuration, i.e. the root file system, the swap
 ;; and the boot partition. Therefore, if the disk fails, we may
 ;; just replace the disk by another one with the same partitioning
 ;; scheme and the same labels; no change of configuration is
 ;; required. On the other hand, the members of the RAID array are
 ;; referenced by their partUUID (a GPT feature).

 (swap-devices (list (swap-space
                      (target (file-system-label "swap")))))

 (mapped-devices
  (list
   (mapped-device
    ;; Use PARTUUID to have unique and unambiguous identifiers
    ;; There is no UUID for RAID members
    (source (list "/dev/disk/by-partuuid/16c45bd4-9758-7b41-9ba9-73ee22434207"
                  "/dev/disk/by-partuuid/f5f84d18-38a2-fd45-a1ff-54ad47539405"
                  "/dev/disk/by-partuuid/b18140ce-2fc6-0e45-b4dd-de46b8628f7a"))
    (target "/dev/md0")
    (type raid-device-mapping))))

 (file-systems (cons* (file-system
                       (mount-point "/")
                       (device (file-system-label "root"))
                       (type "btrfs"))
                      (file-system
                       (mount-point "/boot/efi")
                       (device (file-system-label "EFI"))
                       (type "vfat"))
                      (file-system
                       (mount-point "/pool")
                       (device "/dev/md0")
                       (type "ext4")
                       (dependencies mapped-devices))
                      %base-file-systems)))
