(use-modules (guix packages))
(use-modules (guix download))
(use-modules (guix build-system gnu))
(use-modules (gnu packages ncurses))
(use-modules (gnu packages python))
(use-modules ((guix licenses) #:prefix license:))
(use-modules (gnu packages dbm))
(use-modules (gnu packages tls))
(use-modules (gnu packages compression))
(use-modules (gnu packages tcl))
(use-modules (gnu packages readline))
(use-modules (gnu packages libffi))
(use-modules (gnu packages sqlite))
(use-modules (gnu packages pkg-config))
(use-modules (gnu packages xml))

;; Probably the following package isn't fully deterministic

(define-public python-3.11
  (package
    (name "python")
    (version "3.11.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://www.python.org/ftp/python/"
                                  version "/Python-" version ".tar.xz"))
              (sha256
               (base32 "0f2zdwparivy0p71jr24k1qmq3bw1pcn0qhc44w4ygqkgzx7iaqg"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       (list
        "--without-static-libpython"
        ;; "--enable-shared"               ;fails at validate-runpath
        "--with-pkg-config=yes"
        "--with-system-ffi"
        "--with-system-expat")
       #:test-target "test"
       #:tests? #f))
    (home-page "")
    (inputs (list
             gdbm
             openssl
             zlib
             xz
             tk
             tcl
             ncurses
             readline
             libffi
             sqlite
             pkg-config
             expat))
    (native-search-paths
     (list (guix-pythonpath-search-path version)))
    (synopsis "")
    (description "")
    (license license:psfl)))

python-3.11
