#!/bin/bash

set -eux

HOSTNAME=eryngii
LEGACY=
GRUB_DISK=
if test -d /sys/firmware/efi; then
  LEGACY=false
else
  LEGACY=true
  read -rp 'On which disk will GRUB be installed?'
  GRUB_DISK="$REPLY"
fi

# Set hostname
echo $HOSTNAME > /etc/hostname

# Set root password
passwd

## Fstab
##
## NOTE: the best here would be to provide a description of the 
## partitioning scheme and operate according to that scheme

cat <<EOF > /etc/fstab
LABEL=root /     ext4  defaults,noatime,discard 0 1
LABEL=boot /boot ext4  defaults,noatime,discard 0 2
LABEL=home /home ext4  defaults,noatime,discard 0 2
LABEL=swap swap  swap  rw,noatime,discard 0 0
tmpfs      /tmp  tmpfs defaults,nosuid,nodev 0 0
EOF
# If EFI boot
if ! $LEGACY; then
  echo 'LABEL=EFI /boot/efi vfat rw,relatime 0 1' >> /etc/fstab
fi

# Use hostonly for dracut to generate smaller images. Without hostonly, boot
# freezes after "Loading initial ramdisk". See
# https://unix.stackexchange.com/questions/671087/ubuntu-21-04-hangs-at-loading-initial-ramdisk-after-kernel-update-with-luks
echo 'hostonly=yes' > /etc/dracut.conf.d/00-hostonly.conf

xbps-install -S
# If legacy
if $LEGACY; then
  xbps-install patch grub
  patch /etc/default/grub grub.patch
  grub-install "$GRUB_DISK"
else
  xbps-install patch grub-x86_64-efi
  patch /etc/default/grub grub.patch
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Void
fi

xbps-reconfigure -fa
