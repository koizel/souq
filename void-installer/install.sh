#!/bin/bash

set -eux

REPO=https://repo-de.voidlinux.org/current/musl
ARCH=x86_64-musl

XBPS_ARCH=$ARCH xbps-install -S -R "$REPO" -r /mnt base-system f2fs-tools cryptsetup lvm2

mkdir -p /mnt/var/db/xbps/keys
cp -f /var/db/xbps/keys/* /mnt/var/db/xbps/keys/

cp -f install-xchroot.sh /mnt/
cp -f valuables/grub.patch /mnt/
xchroot /mnt /bin/bash install-xchroot.sh
