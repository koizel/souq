#!/bin/bash

## This script should be idempotent

set -eux
set -o pipefail

enable-service () {
  ln -sf /etc/sv/"$1" /var/service
}

xbps-install void-repo-nonfree
xbps-install -S
xbps-install intel-ucode

## Setting locale

## Adding a user
USERNAME=g
id -u $USERNAME || useradd $USERNAME
usermod -aG wheel,audio,video,lp,input,users $USERNAME

## Visudo
printf 'Run visudo to grant privileges to the new user\n'

## SSH server

## Cron
xbps-install cronie
enable-service cronie

## Timezone
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
xbps-install chrony
enable-service chronyd

## Battery
# elogind?
xbps-install tlp
enable-service tlp

## Wireless
xbps-install connman
enable-service connmand

## Seat management
xbps-install elogind dbus-elogind dbus-elogind-libs dbus-elogind-x11
enable-service elogind
enable-service dbus
# REVIEW should we disable acpid?

## Greeter
xbps-install lightdm lightdm-webkit2-greeter # Chose some greeter


## X and friends
xbps-install xorg intel-video-accel xrandr xclip \
	pipewire wireplumber ratpoison spectrwm st \
	xdg-user-dirs xdg-utils dex xidle unclutter-xfixes \
	zoxide direnv fzf rofi alacritty \
	imv \
	vdirsyncer khal khard \
	mutt urlscan w3m
# Some fonts
xbps-install ipafont-fonts-otf font-hanazono
# Set workman layout in X
mkdir -p /etc/X11/xorg.conf.d/
cp valuables/60-keyboard.conf /etc/X11/xorg.conf.d/
# Auto switch screens (using udev)
xbps-install eudev
enable-service udevd
cp valuables/screen-switch /usr/local/bin/
cp valuables/95-screen-switch.rules /etc/udev/rules.d/

## Bluetooth
xbps-install bluez libspa-bluetooth
enable-service bluetoothd

## User style disk mounting
xbps-install udisks2
