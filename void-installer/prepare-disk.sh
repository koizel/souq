#!/bin/sh

DISK=/dev/vda
LEGACY=true

if $LEGACY; then
  echo 'label: dos' | sfdisk $DISK
  # A boot partition of 800 MiB, and the rest for LVM on LUKS
  printf 'size=800MiB, type=L\nsize=+, type=V\n' | sfdisk $DISK
else
  echo 'label: gpt' | sfdisk $DISK
  # The UEFI partition, the boot partition and the rest for LVM on LUKS
  printf 'size=200MiB, type=U\nsize=800MiB, type=L\nsize=+, type=V\n' | sfdisk $DISK
fi

# Encrypt and open
# pvcreate
# vgcreate
# lvcreate
# mkswap /dev/mapper/vg0-swap
# mkfs.ext2 ...
# mkfs.f2fs /dev/mapper/vg0-root
# mkfs.f2fs /dev/mapper/vg0-home
# mount LABEL=root /mnt
# mkdir -p /mnt/home
# mkdir -p /mnt/boot
# mount LABEL=home /mnt/home
# mount LABEL=boot /mnt/boot
## If efi
# mkdir -p /mnt/boot/efi
# mount LABEL=EFI /mnt/boot/efi
# fi
